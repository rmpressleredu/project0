Richard Pressler
richard@richardpressler.com OR rmpressler@gmail.com
Goffstown, NH - based developer

project0.zip contains the entirety of the Three Aces website as developed by Richard Pressler

This website was a project assigned to students of Harvard's CS75 course: Creating Dynamic Websites. The assignment was to take the menu and turn it into a website at which people could order from Three Aces pizza, an actual pizza shop that went out of business in Connecticut.
Project Requirements: http://cdn.cs75.net/2012/summer/projects/0/project0.pdf
Menu: http://cdn.cs75.net/2012/summer/projects/0/menu.pdf

Site File Structure:

/html
    index.php - main controller
    orderAction.php - contains all actions to modify the SESSION variable $cart
    [various CSS files]
/includes
    helpers.php - contains various helper functions and the OrderItem class
/views
    checkout.php
    home.php
    myOrder.php
    /templates
        header.php
        footer.php
/model
    menu.xml - Database containing all of the order items available on the web site
/deprecated
    admin.php - Admin page that can 1) add new items to menu.xml via a form, and 2) reset the SESSION
    addEntry.php - Handles the insertion of new menu items.
    kill_session.php - Clears SESSION.
    **********Files in the deprecated folder are not included on the live website for security reasons.
