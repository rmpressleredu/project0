<?php
    if (!isset($css)) {
        $css = "main.css";
    }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Three Aces - <?php echo strtr($title, "_", " "); ?></title>
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700'>
  <link rel='stylesheet' type='text/css' href='bootstrap.css'>
  <link rel='stylesheet' type='text/css' href='<?php echo $css; ?>'>
</head>
<body>
<div class='jumbotron'>
  <div class='container'>
    <h1>Three Aces</h1>
      <p>Restaurant - Pizza - Grinders</p>
    </div>
</div>
<div class='nav'>
  <div class='container'>
    <div class='row'>
      <div class='col-md-12'>
        <ul class='nav nav-pills'>
          <li><a href='index.php?page=home'>Home</a></li>
<?php
    $menu = new SimpleXMLElement("../model/menu.xml", 0, true);
    foreach ($menu->category as $category) {
        echo "<li><a href='index.php?page=" . strtr((string)$category->type, " ", "_") . "'>" . (string)$category->type . "</a></li>";
    }
?>
          <li><a class='orderNav' href='index.php?page=myOrder'>My Order</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
