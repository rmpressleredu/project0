<h1>My Order</h1>
<?php
/****************
*myOrder is a view that will load in index.php
*when a user adds to their order (after running through orderAction.php)
*and when a user clicks on "My Order" in the nav bar.
*
*myOrder is responsible for displaying errors passed to it from 
*orderAction.php via $_GET['error']
*****************/

    $menu = new SimpleXMLElement("../model/menu.xml", 0, true);

    if (!isset($_SESSION['cart'])) {
        echo "You haven't added anything to your order yet!";
        exit;
    }
    
    //Check for an error argument and display appropriate message.
    extract($_GET);
    if (isset($error)) {
        switch ($error) {
            case "nan":
                echo "<div class='orderText'><h4>Please enter a numeric quantity!</h4></div>";
                break;
            case "nosize":
                echo "<div class='orderText'><h4>Please select a size!</h4></div>";
                break;
            case "exists":
                echo "<div class='orderText'><h4>
                You've already added this item. To increase the quantity, please do so using your order page below!
                </h4></div>";
                break;
            case "negative":
                echo "<div class='orderText'><h4>Order quantities must be positive.</h4></div>";
                break;
        }
    }
    
    //Check for an $action argument and display confirmation notice if found.
    if (isset($action)) {
        if ($action == "add") {
            echo "<div class='orderText'><h4>Added " . $order_desc . " to your order!</h4></div>";
        }
        else if ($action == "remove") {
            echo "<div class='orderText'><h4>Removed " . $order_desc . " from your order!</h4></div>";
        }
    }

    extract($_SESSION);

    //Loop through $cart contents, showing each item as a separate thumbnail.
    $totalPrice = 0.00;
    echo "<div class=\"row\">";
    for($i = 0; $i < count($cart); $i++) {
        //get category index
        $cat = getIndex($cart[$i]->type, $menu);

        //get order index
        $order = 0;
        for ($x = 0; $x < $menu->category[$cat]->count(); $x++) {
            if ((string)$menu->category[$cat]->order[$x]->name == $cart[$i]->name) {
                $order = $x;
                break;
            }
        }
        
        //Display associated information
        echo "<div class='col-md-3'><div class='thumbnail'>";
        echo "<h2>" . $cart[$i]->type . "</h2>";
        $currentItem = $menu->category[$cat]->order[$order]; //$currentItem points to node of the <order> item in menu.xml
        echo "<img src='" . $currentItem->image . "'>";
        if ($cart[$i]->size == "Only") {
            echo "<h3>" . $currentItem->name . "</h3>";
        }
        else {
            echo "<h3>" . $cart[$i]->size . " " . $currentItem->name . "</h3>";
        }
        //find the corresponding <size> in menu.xml and display the relevant <price>
        //increment $totalPrice by this item's price * quantity
        foreach ($currentItem->sizes->size as $thisSize) {
            if ((string)$thisSize->name == $cart[$i]->size) {
                $totalPrice += floatval($thisSize->price) * $cart[$i]->qty;
                printf("<h5>Price: $%.2f</h5>", floatval($thisSize->price) * $cart[$i]->qty);
            }
        }
        
        //Generate buttons for changing $cart[$i]->qty and removing items. $i holds the $cart index for the given item.
        echo "<form action='orderAction.php' method='get'>";
        echo "Quantity: <input type='text' name='qty' value='" . $cart[$i]->qty . "' size='2'>  ";
        echo "<input type='submit' name='action' class='btn btn-primary' value='Change'>";
        echo "<input type='hidden' name='index' value='" . $i . "'>";
        echo "<input type='submit' name='action' class='btn btn-primary' value='Remove'>";
        echo "</form>";
        echo "</div>";
        echo "</div>";
    }
    echo "</div>";
    
    //Show order total and give Place Order button.
    echo "<div class='row'>";
    echo "<div class='col-md-12'>";
    echo "<div class='total'>";
    printf("<form class='form-horizontal' action='index.php?page=checkout&price=%.2f' method='post'>", $totalPrice);
    printf("<h3>Total = $%.2f</h3>", $totalPrice);
    echo "<input type='submit' class='btn btn-primary pull-right' value='Place Order'>";
    echo "</form>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
?>
