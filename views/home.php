<div class="row">
  <div class="col-md-4">
    <h3>Contact Us</h3>
    <p>(617) 491-2884
      <br>1613 Massachusetts Ave.
      <br>Cambridge, MA 02139</p>
  </div>
  <div class="col-md-4">
    <h2>Order online today!</h2>
  </div>
  <div class="col-md-4">
    <h3>Hours of Operation</h3>
    <h4>Sunday through Thursday</h4>
    <p>10:30 AM - 11:00 PM</p>
    <h4>Friday and Saturday</h4>
    <p>10:30 AM to 2:00 AM</p>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-12">
    <img src="jumbotron.jpg" class="img-responsive">
  </div>
</div>

