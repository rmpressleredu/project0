<?php
    /************************
    *index.php is the primary controller for Three Aces. All other pages render 
    *within index.php by way of its $_GET['page'] variable.
    *************************/
    
    require("../includes/helpers.php"); 
    session_start();

    //Check to see what page was requested, store it in $page. Default to "home"
    if (!isset($_GET['page']) || $_GET['page'] == "home") {
        $page = "home";
    }
    else {
        $page = $_GET['page'];
    }

    //Render header using passed in $page as title
    render("header", array('title' => $page, 'css' => './main.css')); ?>


<div class="content">
    <div class="container">

<?php

    //Render requested page; home, myOrder, and checkout are special cases.
    if ($page == "home") {
        render("home");
    }
    else if ($page == "myOrder") {
        render("myOrder");
    }
    else if ($page == "checkout") {
        render("checkout");
    }
    else {
        getMenu(strtr($page, array('_' => ' ')));
    }

?>

    </div>
</div>

<?php render("footer"); ?>
