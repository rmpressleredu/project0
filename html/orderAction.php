<?php
    /******************************
    *orderAction.php handles adding order items to, and changing order items in, a user's $cart SESSION variable.
    *Also handles all error checking involved in these operations.
    *******************************/

    require("../includes/helpers.php");
    session_start();
    
    //The following selects whether the page should use www.richardpressler.com (live site) or www.threeaces.com (local test)
    //Must set /etc/hosts to map www.threeaces.com/ to localhost.
    
    //Test environment:
    //$location = "Location: http://www.threeaces.com/html/index.php?page=myOrder";
    //Live environment:
    $location = "Location: http://www.richardpressler.com/threeaces/index.php?page=myOrder";
    
    //Validate $qty value
    extract($_GET);
    if (!is_numeric($qty)) {
        header($location . "&error=nan");
        die();
    }
    if ($qty <= 0) {
        header($location . "&error=negative");
        die();
    }
    
    //Prevent fractional orders.
    $qty = (int)$qty;   
  
    //Handle order adding procedure. Throw error if item already in $cart or no $size is selected.
    if ($action == "Add to Order") {
        if ($size == "") {
            header($location . "&error=nosize");
            die();
        }
        //If this is the first item, initialize the $_SESSION['cart'] array.
        if (!isset($_SESSION['start'])) {
            $_SESSION['start'] = true;
            $_SESSION['cart'] = array(new OrderItem($type, $name, $size, $qty));
        }
        //On subsequent additions, if item is not already in cart, push onto array. Else, throw error.
        else {
            foreach ($_SESSION['cart'] as $cartItem) {
                if ($cartItem->name == $name && $cartItem->size == $size && $cartItem->type == $type) {
                    header($location . "&error=exists");
                    die();
                }
            }
            array_push($_SESSION['cart'], new OrderItem($type, $name, $size, $qty));
        }

        //Populate $order with a string containing the item added, including quantity.
        if ($size == "Only") {
            $order = $qty . " " . $name . " " . $type;
        }
        else {
            $order = $qty . " " . $size . " " . $name . " " . $type;
        }

        //Send user back to myOrder.php with message indicating item has been added.
        header($location . "&action=add&order_desc=" . $order);
        die();
    }
    
    //Handle order removing procedure.
    else if ($action == "Remove" || $qty == 0) {
        $order = $_SESSION['cart'][$index]->size . " " . $_SESSION['cart'][$index]->name . " " . $_SESSION['cart'][$index]->type;
        unset($_SESSION['cart'][$index]);
        $_SESSION['cart'] = array_values($_SESSION['cart']);

        header($location . "&action=remove&order_desc=" . $order);
    }
    
    //Handle quantity changing procedure.
    else if ($action == "Change") {
        $_SESSION['cart'][$index]->qty = $qty;
        header($location);
    }
?>
