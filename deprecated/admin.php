<?php require("helpers.php"); ?>

<?php render("header", array('title' => 'Admin Access')); ?>

<div class="col-md-3">
	<h2>Add New Menu Item</h2>
	<form class="form-block" action="addEntry.php" method="post">
		<div class="form-group">
		  <label for="type">Select Type:</label>
		  <select name="type">
		    <?php getTypes(); ?>
		  </select>
		</div>
		<div class="form-group">
		  <label for="name">Name of new offerring:</label>
		  <input type="text" class="form-control" name="orderName">
		</div>
    <div class="form-group">
      <label for="img">Enter URL of image to use:</label>
      <input type="text" class="form-control" name="imgURL">
    </div>
    <div class="form-group">
      <label for="desc">Enter description (optional):</label>
      <input type="text" class="form-control" name="desc">
    </div>
    <div class="form-group">
      <label for="size">Prices for sizes offered:</label>
      <div class="form-inline">
        <label for="only">Only $</label>
        <input type="text" class="form-control" name="onlyPrice">
      </div>
      <div class="form-inline">
        <label for="only">Small $</label>
        <input type="text" class="form-control" name="smallPrice">
      </div>
      <div class="form-inline">
        <label for="only">Medium $</label>
        <input type="text" class="form-control" name="mediumPrice">
      </div>
      <div class="form-inline">
        <label for="only">Large</label>
        <input type="text" class="form-control" name="largePrice">
      </div>
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-primary" value="Add Menu Item">
    </div>
	</form>
</div>

<div class="col-md-3">
  <h2>Reset Session</h2>
    <form action="kill_session.php" method="post">
    <div class="form-group">
      <input type="submit" class="btn btn-primary" value="Reset">
    </div>
    </form>
</div>

<?php render("footer"); ?>
