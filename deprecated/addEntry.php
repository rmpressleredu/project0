<!--Style Complete-->
<?php
    require_once("helpers.php");
    extract($_POST);
    /*
    $type = will hold Pizza, Calzones, etc.
        Use to identify which menu/category to be in
    $orderName = will hold string containing user-entered name of new offering.
        Store in menu/category/order/name
    $imgURL = will hold user-entered image URL.
    Store in menu/category/order/image
    $onlyPrice
    $smallPrice
    $mediumPrice
    $largePrice = These will identify which sizes are available and their prices.
        For each, if there is a value, then that size should be created as a child and the price entered.
    */

    $menu = new SimpleXMLElement('./menu.xml', 0, true); //open xml doc for editing

    $cat = getIndex($type, $menu);

    //Validate entry of sizes.
    if (!$onlyPrice==''
            && (!$smallPrice==''
            || !$mediumPrice==''
            || !$largePrice=='')) {
        echo "Cannot be both 'Only Price' and another!";
        exit;
    }

    //Add entry
    //---------
    //Add <order> to appropriate <category> and set the <name> child element.
    $menu->category[$cat]->addChild('order')->addChild('name', $orderName);

    //Get the index of the new <order> and store it in orderIndex.
    $orderIndex = $menu->category[$cat]->count() - 2;

    //Set <image> child element.
    $menu->category[$cat]->order[$orderIndex]->addChild('image', $imgURL);

    if (!$desc=='') {
        //Set <description> child element.
        $menu->category[$cat]->order[$orderIndex]->addChild('description', $desc);
    }

    //Set sizes and prices.
    $menu->category[$cat]->order[$orderIndex]->addChild('sizes');
    if (!$onlyPrice=='') {
        $menu->category[$cat]->order[$orderIndex]->sizes->addChild('size')->addChild('name', 'Only');
        $menu->category[$cat]->order[$orderIndex]->sizes->size->addChild('price', $onlyPrice);
    }
    else {
        if (!$smallPrice=='') {
            $menu->category[$cat]->order[$orderIndex]->sizes->addChild('size')->addChild('name', 'Small');
            $curIndex = $menu->category[$cat]->order[$orderIndex]->sizes->size->count() - 1;
            $menu->category[$cat]->order[$orderIndex]->sizes->size[$curIndex]->addChild('price', $smallPrice);
        }
        if (!$mediumPrice=='') {
            $menu->category[$cat]->order[$orderIndex]->sizes->addChild('size')->addChild('name', 'Medium');
            $curIndex = $menu->category[$cat]->order[$orderIndex]->sizes->size->count() - 1;
            $menu->category[$cat]->order[$orderIndex]->sizes->size[$curIndex]->addChild('price', $mediumPrice);
        }
        if (!$largePrice=='') {
            $menu->category[$cat]->order[$orderIndex]->sizes->addChild('size')->addChild('name', 'Large');
            $curIndex = $menu->category[$cat]->order[$orderIndex]->sizes->size->count() - 1;
            $menu->category[$cat]->order[$orderIndex]->sizes->size[$curIndex]->addChild('price', $largePrice);
        }
    }

    //Export change.
    $menu->asXml('menu.xml');

    echo "Change successful! Click <a href='admin.php'>here</a> to go back!";
?>
