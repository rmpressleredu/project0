<?php
    /******************************
    *OrderItem class is used to store individual order
    *components in the $cart SESSION variable.
    *******************************/
    class OrderItem {
        public $type; //Must match a menu->category->type in menu.xml
        public $name; //Must match a menu->category->order within <category> stored in $type
        public $size; //Must match a menu->category->order->sizes->size within <order> stored in $name
        public $qty;  //Must be a non-negative integer

        public function __construct ($newType, $newName, $newSize, $newQty) {
            $this->type = $newType;
            $this->name = $newName;
            $this->size = $newSize;
            $this->qty = $newQty;
        }
    }

    /*******************************
    *void render(string $template, array $data)
    *
    *Used to render a passed $template.php file.
    *
    *@param string $template must be a valid filename in either /views or /views/templates.
    *                        Should NOT include a file extension. This is added by the function and is always .php
    *@param array $data may contain any number of parameters, specific to the page being loaded.
    ********************************/
    function render($template, $data = array()) {
        $path = $template . ".php";
        if (file_exists("../views/" . $path)) {
            extract($data);
            require("../views/" . $path);
        }
        else if(file_exists("../views/templates/" . $path)) {
            extract($data);
            require("../views/templates/" . $path);
        }
        else {
            echo "Failed calling render(). $path doesn't exist!";
            exit;
        }
    }

    /*******************************************
    *int getIndex(string $request, SimpleXMLElement $xml)
    *
    *Returns the numeric index indicating the <category> for the requested type of food. If not found, returns -1.
    *
    *@param string $request Contains the string to be found in the menu->category->type element
    *@param SimpleXMLElement $xml contains the open connection to menu.xml
    ********************************************/
    function getIndex($request, $xml) {
        $reqIndex = -1;
        for ($i = 0; $i < $xml->count();$i++) {
            if ($xml->category[$i]->type == $request) {
                $reqIndex = $i;
                break;
            }
        }
        return $reqIndex;
    }

    /*******************************************
    *void getMenu(string $type)
    *
    *Renders a thumbnail grid of menu items contained within a given
    *category in menu.xml
    *
    *@param string $type contains a string that must match a menu->category->type in menu.xml
    ********************************************/
    function getMenu($type) {
        $menu = new SimpleXMLElement("../model/menu.xml", 0, true);

        $cat = getIndex($type, $menu);
        echo "<h1>{$menu->category[$cat]->type}</h1>";
 
        $colCount = 0; //Keeps track of columns, used to detect when to create new row; 4 columns.
        echo "<div class='row'>";
        
        //Loop through all children within the selected <category>
        foreach ($menu->category[$cat]->children() as $menuitem) {
            if ($colCount == 4) {
                echo "</div><div class='row'>";
                $colCount = 0;
            }
            
            //Skip over the <type> child
            if ((string)$menuitem == (string)$menu->category[$cat]->type) {
                continue;
            }

            //Send <image> and <name>
            echo "<div class='col-md-3'><div class='thumbnail'>";
            echo "<img src='{$menuitem->image}'>";
            echo "<h3>{$menuitem->name}</h3>";

            if ($menuitem->description) {
                echo "<p>{$menuitem->description}</p>";
            }

            //Send <sizes>
            echo "<form class='form-horizontal' action='orderAction.php' method='get'>";
            echo "<h4>Pick a size:</h4>";
            foreach ($menuitem->sizes->children() as $itemsize) {
                echo "<input type='radio' name='size' value='" . $itemsize->name . "'>";
                echo $itemsize->name . " - $" . $itemsize->price;
                echo "</input><br>";
                if ((string)$itemsize->name == "Only") {
                    echo "<br>";
                }
            }

            //Send quantity field and submit button.
            echo "<div class='form-inline'>
                    <div class='form-group'>
                        <label class='control-label col-md-5'>Quantity: </label>
                        <div class='col-md-3'>
                            <input type='text' class='form-control' name='qty' value='1' size='1'>
                        </div>
                    </div>";
            echo "<input type='hidden' name='type' value='{$menu->category[$cat]->type}'>";
            echo "<input type='hidden' name='name' value='{$menuitem->name}'>";
            echo "<div class='form-group'>
                    <input type='submit' class='btn btn-primary' name='action' value='Add to Order'>
                  </div></div>";
            echo "</form></div></div>";
            $colCount++;
        }
        echo "</div>";
    }

    /*********************************
    *void getTypes(void)
    *
    *Used to populate the dropdown menu in admin.php. Iterates through menu.xml, adding a menu option for each <category>
    *
    *Not used in official release - admin.php only intended to be tool to aid in development.
    **********************************/
    function getTypes() {
        $menu = new SimpleXMLElement("./menu.xml", 0, true);
        $types = array();

        foreach ($menu->children() as $category) {
            echo "<option value='" . (string)$category->type . "'>" . (string)$category->type . "</option>";
        }
    }
?>
